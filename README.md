RSSReader
=========

RSS Reader Application using SqlLite database...........

In this tutorial i am going to explain building simple rss reader application.

The is very simple rss reader application. The complete application will have four views
1. A view will list all the websites (stored in SQLite table) in a listview
2. View to add new website to application.
3. On selecting single website from listview another listview will display list of articles for that particular website
4. Once the sinlge article is selected that web page will display on webview.
SQLite database is used to store websites entered by used.

Downloading Jsoup html parser library
In order to get rss links from html source code i used Jsoup java library. Download and add the jar file to your project Java Build Path library.

You can download the jar file by going to http://jsoup.org/download

Once downloaded Right Click on project -> properties -> Select Java Build Path (on left side) -> Select third tab Libraries (on right side) -> Add External Jar -> Browse to downloaded jsoup .jar file.

